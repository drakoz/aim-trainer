using UnityEngine;
using UnityEngine.UI;

public class DynamicSlider : MonoBehaviour
{
    public Slider slider;

    public void ChangeValue(string newValue)
    {
        if (newValue == "")
        {
            newValue = slider.minValue.ToString();
        }
        slider.value = Mathf.Round(float.Parse(newValue) * 100f) / 100f;
    }
}
