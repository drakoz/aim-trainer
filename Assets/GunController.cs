using UnityEngine;

public class GunController : MonoBehaviour
{
    public Camera fpsCam;
    public ParticleSystem muzzleFlash;
    private bool isMenuOpen = false;
    AudioSource fireSound;

    void Start()
    {
        fireSound = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1") && !isMenuOpen)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        muzzleFlash.Play();
        fireSound.Play();
    
        RaycastHit hit;

        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit))
        {
            Debug.Log(hit.transform.name);

            Target target = hit.transform.GetComponent<Target>();
            if (target != null)
            {
                target.Die();
            }
        }
    }

    public void OpenCloseMenu()
    {
        isMenuOpen = !isMenuOpen;
    }
}
