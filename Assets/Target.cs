using UnityEngine;

public class Target : MonoBehaviour
{
    AudioSource targetHit;

    void Start()
    {
        targetHit = GetComponent<AudioSource>();
    }

    public void Die()
    {
        targetHit.Play();
        Destroy(gameObject);
    }
}
