using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MouseController : MonoBehaviour
{
    public float mouseSensitivity = 100f;
    public float sensitivityMultiplier = 1f;
    public Transform playerBody;
    public GameObject menu;
    public UnityEvent onMenuOpen;
    float xRotation = 0f;

    private bool isMenuOpen = false;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !isMenuOpen)
        {
            OpenMenu();
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape) && isMenuOpen)
        {
            CloseMenu();
            return;
        }

        if (!isMenuOpen)
        {
            float mouseX = Input.GetAxis("Mouse X") * sensitivityMultiplier * mouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * sensitivityMultiplier * mouseSensitivity * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            playerBody.Rotate(Vector3.up * mouseX);
        }

    }

    public void ChangeSensitivity(float sensitivity)
    {
        sensitivityMultiplier = Mathf.Round(sensitivity * 100f) / 100f;
    }

    void OpenMenu()
    {
        menu.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        isMenuOpen = true;
        onMenuOpen.Invoke();
    }

    void CloseMenu()
    {
        menu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        isMenuOpen = false;
        onMenuOpen.Invoke();
    }
}
