using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject targetObj;
    GameObject target;
    public Target start;
    private bool isPlaying = false;
    private bool returnToCenter = false;
    private Bounds bounds;

    void Start()
    {
        bounds = GetComponent<Collider>().bounds;
    }

    // Update is called once per frame
    void Update()
    {
        if (start == null && !isPlaying)
        {
            target = Instantiate(targetObj, RandomPoint(), this.transform.rotation);
            isPlaying = true;
            returnToCenter = true;
        }

        if (target == null && isPlaying && !returnToCenter)
        {
            target = Instantiate(targetObj, RandomPoint(), this.transform.rotation);
            returnToCenter = true;
        }

        if (target == null && isPlaying && returnToCenter)
        {
            target = Instantiate(targetObj, this.transform);
            returnToCenter = false;
        }
    }

    Vector3 RandomPoint()
    {
        return new Vector3(Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z));
    }
}
