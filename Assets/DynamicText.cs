using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DynamicText : MonoBehaviour
{
    public TMP_InputField text;
    public Slider slider;

    public void ChangeText(float newText)
    {
        text.text = newText.ToString("F2");
    }

    public void ValidateText()
    {
        if (float.Parse(text.text) > slider.maxValue)
        {
            text.text = "10.00";
        }
    }
}
