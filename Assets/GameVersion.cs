using UnityEngine;
using TMPro;

public class GameVersion : MonoBehaviour
{
    void Start()
    {
        TextMeshProUGUI text = GetComponent<TextMeshProUGUI>();
        text.text = "v" + Application.version;
    }
}
